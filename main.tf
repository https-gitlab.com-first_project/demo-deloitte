# main.tf

provider "aws" {
  region = "us-east-1"
}

# VPC
resource "aws_vpc" "challenge_vpc" {
  cidr_block            = "10.110.0.0/16"
  enable_dns_support    = true
  enable_dns_hostnames  = true
  tags = {
    Name = "vpc-challenge-demo"
  }
}

# Subnets
resource "aws_subnet" "public_subnet_1a" {
  vpc_id                  = aws_vpc.challenge_vpc.id
  cidr_block              = "10.110.10.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "Public-Subnet-1a"
  }
}

resource "aws_subnet" "private_subnet_1a" {
  vpc_id     = aws_vpc.challenge_vpc.id
  cidr_block = "10.110.20.0/23"
  availability_zone = "us-east-1a"
  tags = {
    Name = "Private-Subnet-1a"
  }
}

resource "aws_subnet" "public_subnet_2b" {
  vpc_id                  = aws_vpc.challenge_vpc.id
  cidr_block              = "10.110.30.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true
  tags = {
    Name = "Public-Subnet-2b"
  }
}

resource "aws_subnet" "private_subnet_2b" {
  vpc_id     = aws_vpc.challenge_vpc.id
  cidr_block = "10.110.40.0/23"
  availability_zone = "us-east-1b"
  tags = {
    Name = "Private-Subnet-2b"
  }
}

# Public Route Table for public subnets
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.challenge_vpc.id

  tags = {
    Name = "public-route-table"
  }
}

# Associate public route table with public subnets
resource "aws_route_table_association" "public_subnet_association_1a" {
  subnet_id      = aws_subnet.public_subnet_1a.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_subnet_association_2b" {
  subnet_id      = aws_subnet.public_subnet_2b.id
  route_table_id = aws_route_table.public_route_table.id
}

# Private Route Table for private subnets
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.challenge_vpc.id

  tags = {
    Name = "private-route-table"
  }
}

# Associate private route table with private subnets
resource "aws_route_table_association" "private_subnet_association_1a" {
  subnet_id      = aws_subnet.private_subnet_1a.id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_route_table_association" "private_subnet_association_2b" {
  subnet_id      = aws_subnet.private_subnet_2b.id
  route_table_id = aws_route_table.private_route_table.id
}


# Security Groups
resource "aws_security_group" "presentation_tier_sg" {
  name        = "Presentation-Tier-SG"
  description = "Security Group for Presentation Tier"
  vpc_id      = aws_vpc.challenge_vpc.id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["147.235.206.12/32"]
  }
}

resource "aws_security_group" "logic_tier_sg" {
  name        = "Logic-Tier-SG"
  description = "Security Group for Logic Tier"
  vpc_id      = aws_vpc.challenge_vpc.id

  ingress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"
    cidr_blocks = ["10.110.20.0/23"]
  }

  ingress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"
    cidr_blocks = ["10.110.40.0/23"]
  }

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    cidr_blocks = ["10.110.40.0/23"]
  }

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    cidr_blocks = ["10.110.20.0/23"]
  }

  ingress {
    from_port = 8393
    to_port   = 8393
    protocol  = "tcp"
    cidr_blocks = ["10.110.40.0/23"]
  }

  ingress {
    from_port = 8393
    to_port   = 8393
    protocol  = "tcp"
    cidr_blocks = ["10.110.20.0/23"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["10.111.10.0/24"]
  }

  
}

resource "aws_security_group" "data_tier_sg" {
  name        = "Data-Tier-SG"
  description = "Security Group for Data Tier"
  vpc_id      = aws_vpc.challenge_vpc.id

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    cidr_blocks = ["10.110.40.0/23"]
  }

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    cidr_blocks = ["10.110.20.0/23"]
  }
}

# Internet Gateway
resource "aws_internet_gateway" "demo_gw" {
  vpc_id = aws_vpc.challenge_vpc.id

  tags = {
    Name = "demo-gw"
  }
}

# Elastic IP for NAT Gateway
resource "aws_eip" "nat_eip" {
  instance = aws_instance.web_instance_with_efs.id
}

# NAT Gateway
resource "aws_nat_gateway" "nat_gateway_demo" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet_1a.id

  tags = {
    Name = "Nat-GW-Demo"
  }
}

# Route Table for private subnet 1a with NAT Gateway
resource "aws_route_table" "private_route_table_1a" {
  vpc_id = aws_vpc.challenge_vpc.id

  tags = {
    Name = "private-route-table-1a"
  }
}

# Route for NAT Gateway in private route table 1a
resource "aws_route" "route_to_nat_gateway_private_1a" {
  route_table_id         = aws_route_table.private_route_table_1a.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway_demo.id
}

# Route Table for private subnet 2b with NAT Gateway
resource "aws_route_table" "private_route_table_2b" {
  vpc_id = aws_vpc.challenge_vpc.id

  tags = {
    Name = "private-route-table-2b"
  }
}

# Route for NAT Gateway in private route table 2b
resource "aws_route" "route_to_nat_gateway_private_2b" {
  route_table_id         = aws_route_table.private_route_table_2b.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway_demo.id

}

# Elastic Load Balancer
resource "aws_lb" "application_lb_demo" {
  name               = "Application-LB-demo"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.presentation_tier_sg.id]
  subnets            = [aws_subnet.public_subnet_1a.id, aws_subnet.public_subnet_2b.id]

  enable_deletion_protection = false
}

# ALB Listener
resource "aws_lb_listener" "web_listener" {
  load_balancer_arn = aws_lb.application_lb_demo.arn
  port              = 443
  protocol          = "HTTPS"

  default_action {
    type             = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code  = "200"
      message_body = "OK"
    }
  }

  ssl_policy = "ELBSecurityPolicy-2016-08"
  certificate_arn = "<Your_SSL_Certificate_ARN>"
}

# ALB Target Group
resource "aws_lb_target_group" "web_target_group" {
  name        = "web-target-group"
  port        = 443
  protocol    = "HTTPS"
  vpc_id      = aws_vpc.challenge_vpc.id

  health_check {
    path = "/health-check"
    port = 443
  }
}

# ALB Listener Rule
resource "aws_lb_listener_rule" "web_listener_rule" {
  listener_arn = aws_lb.application_lb_demo.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }
}


resource "aws_ecs_cluster" "demo_ecs_cluster" {
  name = "demo-ecs-cluster"
}
# Autoscaling Group
resource "aws_autoscaling_group" "web_asg" {
  desired_capacity     = 2
  max_size             = 4
  min_size             = 1
  launch_template {
    id      = "lt-05f3dec845e5e2d84"
    version = "1.0.0"
  }
  vpc_zone_identifier  = [aws_subnet.private_subnet_1a.id, aws_subnet.private_subnet_2b.id]
  health_check_type    = "EC2"
  health_check_grace_period = 300
  tag {
    key                 = "Name"
    value               = "web-instance"
    propagate_at_launch = true
  }
}

# RDS - SQL Server Database
resource "aws_db_instance" "sql_db" {
  identifier               = "sql-db-demo"
  allocated_storage        = 20
  storage_type             = "gp2"
  engine                   = "sqlserver-ex"
  engine_version           = "14.00.3281.6.v1"
  instance_class           = "db.t2.micro"
  username                 = "lovelyuser"
  password                 = "LovelyPassw0rd"
  port                     = 1433
  multi_az                 = false
  publicly_accessible      = false

  vpc_security_group_ids   = [aws_security_group.data_tier_sg.id]
  db_subnet_group_name     = aws_db_subnet_group.sql_db_subnet_group.name
}

# RDS DB Subnet Group
resource "aws_db_subnet_group" "sql_db_subnet_group" {
  name       = "sql-db-subnet-group"
  subnet_ids = [aws_subnet.private_subnet_1a.id, aws_subnet.private_subnet_2b.id]
}

# Key Management Service (KMS)
resource "aws_kms_key" "logic_to_db_key" {
  description             = "KMS key for Logic to Database encryption"
  deletion_window_in_days = 7
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["rds.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "logic_to_db_key" {
  name               = "iam-role-for-grant"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_kms_grant" "logic_to_db_grant" {
  key_id  = aws_kms_key.logic_to_db_key.id
  name    = "grant-logic-to-db"
  grantee_principal = aws_iam_role.logic_to_db_key.arn

  operations = ["Encrypt", "Decrypt", "GenerateDataKey"]
}

# EFS File System
resource "aws_efs_file_system" "demo_efs" {
  creation_token = "efs-demo"
  performance_mode = "generalPurpose"
  throughput_mode = "bursting"
  encrypted = false
  tags = {
    Name = "EFS-Demo"
  }

  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }

  provisioned_throughput_in_mibps = 0.5
}

resource "aws_efs_mount_target" "demo_efs_mount_target_1" {
  file_system_id  = aws_efs_file_system.demo_efs.id
  subnet_id       = aws_subnet.private_subnet_1a.id
  security_groups = [aws_security_group.logic_tier_sg.id]
}

resource "aws_efs_mount_target" "demo_efs_mount_target_2" {
  file_system_id  = aws_efs_file_system.demo_efs.id
  subnet_id       = aws_subnet.private_subnet_2b.id
  security_groups = [aws_security_group.logic_tier_sg.id]
}

resource "aws_instance" "web_instance_with_efs" {
  ami           = "ami-003e86d1538f611cb"
  instance_type = "t2.micro"
  key_name      = "demo-key"
  subnet_id     = aws_subnet.private_subnet_1a.id

  launch_template {
    id      = "lt-05f3dec845e5e2d84"
    version = "1.0.0"
  }

  user_data = <<-EOF
              #!/bin/bash
              yum install -y amazon-efs-utils
              mount -t efs ${aws_efs_file_system.demo_efs.id}:/ /var/www/html/wp-content
              EOF
}

# VPC Endpoint for S3
resource "aws_vpc_endpoint" "s3_endpoint" {
  vpc_id = aws_vpc.challenge_vpc.id

  service_name = "com.amazonaws.us-east-1.s3"
  
  route_table_ids = [aws_route_table.private_route_table_1a.id, aws_route_table.private_route_table_2b.id]
}

# Allow traffic from the VPC to the S3 bucket
resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = "demo-deloitte-buckup"

  policy = jsonencode({
    Version = "2012-10-17",
    Id      = "VPCPolicy",
    Statement = [
      {
        Effect  = "Allow",
        Principal = "*",
        Action  = ["s3:*"],
        Resource = [
          "arn:aws:s3:::demo-deloitte-buckup",
          "arn:aws:s3:::demo-deloitte-buckup/*",
        ],
        Condition = {
          StringEquals = {
            "aws:sourceVpce" = aws_vpc_endpoint.s3_endpoint.id,
          },
        },
      },
    ],
  })
}

# S3 Bucket for buckuping solution
#resource "aws_s3_bucket" "demo_s3" {
#  bucket = "my-demo-in-s3-bucket"

#  tags = {
#    name    = "Web-storage-buckup"
#    Environment = "Demo"

#  }

#}

# ...
