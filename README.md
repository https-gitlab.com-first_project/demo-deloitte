# demo-deloitte

Deloitte Cloud Challenge

Overview

The Deloitte Cloud Challenge project aims to create a secure and scalable cloud infrastructure using AWS (Amazon Web Services). The infrastructure is designed to host a WordPress application with high availability, incorporating best practices for security and compliance.

Key Components
VPC and Subnets:

Configures a Virtual Private Cloud (VPC) with public and private subnets across multiple Availability Zones.
Security Groups:

Defines security groups for different tiers (presentation, logic, data) to control inbound and outbound traffic.
Load Balancer:

Sets up an Application Load Balancer (ALB) to distribute traffic among EC2 instances.
Auto Scaling:

Implements Auto Scaling Groups to automatically adjust the number of EC2 instances based on demand.
RDS (Relational Database Service):

Deploys an RDS instance for hosting a SQL Server database with encryption and secure communication.
EFS (Elastic File System):

Utilizes Amazon EFS for scalable and shared file storage across EC2 instances.
S3 Bucket:

Configures an S3 bucket for backups, with VPC endpoint for private access.

Security and Compliance
The project aligns with the NIST Cybersecurity Framework and implements security controls to safeguard the cloud environment. Regular updates and reviews are recommended to address emerging security threats.

Thanks,

Designed by - Ofir Shmuel
Year - 2024